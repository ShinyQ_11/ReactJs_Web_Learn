import React, {Component} from 'react';

export default class Glass extends Component{

    render(){

        const {ukuranGelas, isiGelas} = this.props;

        return(
            <div>
            <p>Ini Glass</p>
            <ul>
                <li> Ukuran Gelas : {ukuranGelas} </li>
                <li> Isi Gelas {isiGelas} </li>
            </ul>
            </div>
        );
    }
}