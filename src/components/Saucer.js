import React, {Component} from 'react';

export default class Saucer extends Component{

    render(){

        const {ukuranSaucer} = this.props;

        return(
            <div>
            <p>Ini Saucer</p>
            <ul>
                <li>Ukuran Saucer : {ukuranSaucer}</li>
            </ul>
            </div>
        );
    }
}