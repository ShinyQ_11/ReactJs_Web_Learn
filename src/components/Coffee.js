import React, {Component} from 'react';

export default class Coffee extends Component{

    render(){

        const {waterVolume, jenisKopi, bijiKopi} = this.props;

        return(
            <div>
            <p>Ini Coffee</p>
            <ul>
                <li><p> Volume Air : {waterVolume} </p></li>
                <li><p> Bean : {bijiKopi}</p></li>
                <li><p> Jenis Kopi : {jenisKopi} </p></li>
            </ul>
            </div>
        );
    }
}