import React, {Component} from 'react';

export default class Index extends Component{
  
  constructor(){
    super();
    this.state = {
      number:1,
      nama: "kurniadi ahmad wijaya"
    }
  }


  handleClick(){
    const number = this.state.number + 1
    this.setState({
      number
    })
  }
  
  render(){
    return(
      <div>
      <button onClick={()=> this.handleClick()}>Tambah Angka</button>
      <p>{this.state.number}</p>
      </div>
    );
  }
}